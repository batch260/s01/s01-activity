<?php 




function getFullAddress($roomNum,$buildingName, $block,$province,$city,$country) {
	return "$roomNum $buildingName, $block, $province, $city, $country";
}




function getLetterGrade($grade) {
	if ($grade <= 74) {
		return 'is equivalent to F';
	} else if ($grade >= 75 && $grade <= 76) {
		return 'is equivalent to C-';
	} else if ($grade >= 77 && $grade <= 79) {
		return 'is equivalent to C';
	} else if ($grade >= 80 && $grade <= 82) {
		return 'is equivalent to C+';
	} else if ($grade >= 83 && $grade <= 85) {
		return 'is equivalent to B-';
	} else if ($grade >= 86 && $grade <= 88) {
		return 'is equivalent to B';
	} else if ($grade >= 89 && $grade <= 91) {
		return 'is equivalent to B+';
	} else if ($grade >= 92 && $grade <= 94) {
		return 'is equivalent to A-';
	} else if ($grade >= 95 && $grade <= 97) {
		return 'is equivalent to A';
	} else if ($grade >= 98 && $grade <= 100) {
		return 'is equivalent to A+';
	} else{
		return 'Repeat the Subject';
	}
}

