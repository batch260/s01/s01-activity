<!-- PHP code can be included to another file by using the 'require_once' keyword -->
<!-- We will use this method to separate the declaration of variables and functions from the HTML content. -->
<?php require_once "./code.php"; ?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ACTIVITY s01</title>
</head>
<body>

	<h1>Full Address</h1>
	<p><?php echo getFullAddress('3F', 'Caswynn Bldg.', 'Timog Avenue', 'Quezon City', 'Metro Manila', 'Philippines'); ?></p>
	<p><?php echo getFullAddress('3F', 'Enzo Bldg.', 'Buendia Avenue', 'Makati City', 'Metro Manila', 'Philippines'); ?></p>

	<br>
	<br>
	<br>
	<h1>Letter-Based Grading</h1>
	<p>87 <?php echo getLetterGrade(87); ?></p>
	<p>94 <?php echo getLetterGrade(94); ?></p>
	<p>74 <?php echo getLetterGrade(50); ?></p>
	
	
</body>
</html>